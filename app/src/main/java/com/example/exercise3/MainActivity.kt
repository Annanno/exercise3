package com.example.exercise3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Toast
import com.example.exercise3.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        init()
    }

    fun init() {
        binding.saveButton.setOnClickListener {
            save()
        }
        binding.clearButton.setOnLongClickListener {
            binding.email.text.clear()
            binding.userName.text.clear()
            binding.firstName.text.clear()
            binding.lastName.text.clear()
            binding.age.text.clear()
            true
        }
    }

    fun save() {
        val email = binding.email.text
        val userName = binding.userName.text
        val firstName = binding.firstName.text
        val lastName = binding.lastName.text
        val age = binding.age.text
        if (email.isEmpty() || userName.isEmpty() || firstName.isEmpty() || lastName.isEmpty() || age.isEmpty())
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_LONG).show()
            else if (userName.length < 10)
                Toast.makeText(this, "Number of username's symbols should be more than 10", Toast.LENGTH_SHORT)
                    .show()
            else if (email.toString().matches(emailPattern.toRegex())) {
                d("msg", "Email is valid")
            } else Toast.makeText(this, "Entered email is not valid", Toast.LENGTH_SHORT).show()

    }
}